package org.tartarus.snowball.ext;

import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

public class OutputTest {
    @Test
    public void testFullDict() throws Exception {
        try (
            Scanner scanner = new Scanner(Path.of("data", "voc.txt"), StandardCharsets.UTF_8);
            PrintWriter writer = new PrintWriter(new FileWriter(new File("data", "output.txt"), StandardCharsets.UTF_8));
        ) {
            yiddishStemmer stemmer = new yiddishStemmer();
            int i = 0;
            while (scanner.hasNextLine()) {
                String word = scanner.nextLine();
                stemmer.setCurrent(word);
                try {
                    stemmer.stem();
                } catch (Exception e) {
                    System.out.println("Exception on " + word);
                    throw e;
                }
                String stem = stemmer.getCurrent();
                writer.println(stem);
                writer.flush();
                i++;
                if (i % 1000 == 0)
                    System.out.println(String.format("Stemmed %d words", i));
            }
        }
    }
}
