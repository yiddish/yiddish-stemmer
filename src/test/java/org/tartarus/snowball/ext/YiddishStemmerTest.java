package org.tartarus.snowball.ext;

import org.junit.Assert;
import org.junit.Test;

public class YiddishStemmerTest {
    yiddishStemmer stemmer = new yiddishStemmer();
    
    @Test
    public void testStemmer() {
        testStem("געאײַלן", "אײל");
        testStem("געאײַלט", "אײל");
        testStem("געבאָדענעם", "באד");
        testStem("געבאָטענעם", "באט");
        testStem("געאַכלט", "אכל");
        testStem("עראָפּלאַנען", "עראפלאנ");
        testStem("קינדהײט", "קינד");
        testStem("ביכער", "ביכ");
        testStem("פֿאַרגאַנגענהײט", "פארגאנגענ");
        testStem("װילן", "װיל");
        testStem("װילסט", "װיל");
        testStem("אַבסטראַקטסטער", "אבסטראקט");
        testStem("אַבֿידות", "אבידה");
        testStem("אַדורכבײַסנדיקער", "אדורכבײס");
        testStem("אַדורכגעשמועסט", "אדורכשמוע");
        testStem("באַהאַלטן", "באהאל");
        testStem("אַדורכפֿירנדיק", "אדורכפיר");
        testStem("אַדורכגעביסן", "אדורכבײס");
        testStem("אַװעקגעגאַנגען", "אװעקגײ");
        testStem("אַװעקגעגאַנגענעם", "אװעקגײ");
        testStem("אַװעקגענומענער", "אװעקנעמ");
        testStem("פֿאָרױסגעגאַנגענע", "פארױסגײ");
        testStem("געשדכנטע", "שדכנ");
        testStem("אמתדיק","אמת");
    }
    
    private void testStem(String word, String expectedStem) {
        stemmer.setCurrent(word);
        stemmer.stem();
        String stem = stemmer.getCurrent();
        System.out.println(String.format("Word: %s. Stem: %s", word, stem));
        if (expectedStem != null && expectedStem.length()>0)
            Assert.assertEquals(expectedStem, stem);
    }
}
