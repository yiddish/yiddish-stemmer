# Yiddish stemmer

[Snowball](https://snowballstem.org/) stemmer for Yiddish in YIVO spelling.

The stemmer code is maintained in the [Snowball source code](src/main/snowball/yiddih.sbl).
All of the files in the [src/main/java](src/main/java) are either copied from the Snowball project or automatically generated.

If you find words you feel are stemmed incorrectly, please don't hesitate to raise an issue.

## Compiling the snowball stemmer

If you change the snowball code, you need to recompile the snowball stemmer.
 
Download and build snowball as described [here](https://snowballstem.org/runtime/use.html).

Next, run the following command:
```
./snowball /path/to/yiddish-stemmer/src/main/snowball/yiddish.sbl -java -o /path/to/yiddish-stemmer/src/main/java/org/tartarus/snowball/ext/YiddishStemmer
```

## Credits

The stemmer was developped by Assaf Urieli.